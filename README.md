# @pencilpix/nuxt-comments
[![npm (scoped with tag)](https://img.shields.io/npm/v/@pencilpix/nuxt-comments/latest.svg?style=flat-square)](https://npmjs.com/package/@pencilpix/nuxt-comments)
[![npm](https://img.shields.io/npm/dt/@pencilpix/nuxt-comments.svg?style=flat-square)](https://npmjs.com/package/@pencilpix/nuxt-comments)
[![CircleCI](https://img.shields.io/circleci/project/github/.svg?style=flat-square)](https://circleci.com/gh/)
[![Codecov](https://img.shields.io/codecov/c/github/.svg?style=flat-square)](https://codecov.io/gh/)
[![Dependencies](https://david-dm.org//status.svg?style=flat-square)](https://david-dm.org/)
[![js-standard-style](https://img.shields.io/badge/code_style-standard-brightgreen.svg?style=flat-square)](http://standardjs.com)

> some helpers components/directives/global methods

[📖 **Release Notes**](./CHANGELOG.md)

## Features

The module features

## Setup
- Add `@pencilpix/nuxt-comments` dependency using yarn or npm to your project
- Add `@pencilpix/nuxt-comments` to `modules` section of `nuxt.config.js`

```js
{
  modules: [
    // Simple usage
    '@pencilpix/nuxt-comments',

    // With options
    ['@pencilpix/nuxt-comments', { /* module options */ }],
 ]
}
```

## Usage

Module Description

## Development

- Clone this repository
- Install dependnecies using `yarn install` or `npm install`
- Start development server using `npm run dev`

## License

[MIT License](./LICENSE)

Copyright (c) Mohamed hassan <pencilpix11@gmail.com>
